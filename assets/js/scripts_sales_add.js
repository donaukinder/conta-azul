function selectClient(obj) {
    var id = $(obj).attr('data-id');
    var name = $(obj).html();
 
    $('.client_search_input').hide();
    $('#client_name').val(name);
    $('input[name=client_id]').val(id);
}
 
function updateSubtotal(obj) {
    var quant = $(obj).val();
    if (quant <= 0) {
        $(obj).val(1);
        quant = 1;
    }
    var price = $(obj).attr('data-price');
    var subtotal = price * quant;
 
    $(obj).closest('tr').find('.subtotal').html('R$ '+subtotal);Ω
}

function updateTotal(){
    var total = 0;

    for(var q=0;q<$('.prod_quant').length;q++){
        var quant = $('.prod_quant').eq(q);
        var price = quant.attr('data-price');
        var subtotal = price * parseInt(quant.val());

        total += subtotal;
    }
    $('input[name=total_price]').val(total);
}
 
function deleteProd(obj){
    $(obj).closest('tr').remove();
}
 
function add_product(obj) {
    $('#add_product').val('');
    var id = $(obj).attr('data-id');
    var name = $(obj).attr('data-name')
    var price = $(obj).attr('data-price');
 
 
    $('.searchresults_prod').hide();
   
    if ($('input[name="quant['+id+']"]').length == 0) {
        var tr =
            '<tr align=center>' +
            '<td>' + name + '</td>' +
            '<td>' +
            '<input type="number" class="prod_quant" name="quant['+id+']" value="1" onchange="updateSubtotal(this)" data-price="' + price + '"/>' +
            '</td>' +
            '<td>R$ ' + price + '</td>' +
            '<td class="subtotal">R$ ' + price + '</td>' +
            '<td><a href="javascript:;" onclick="deleteProd(this)" class="btn btn-danger btn-sm">Excluir</a></td>' +
            '</tr>';
 
        $('#products_table').append(tr);
   
    } else {
 
    }
}
 
$(function () {
    $('input[name=total_price]').mask("000.000.000.000.000,00", { reverse: true, placeholder: "0,00" });
 
    $('.client_add').on('click', function () {
        var name = $('#client_name').val();
        if (name != '' && name.length >= 4) {
            if (confirm('Você deseja um cliente como o nome: ' + name + ' ?')) {
                $.ajax({
                    url: BASE_URL + '/ajax/add_client',
                    type: 'POST',
                    data: { name: name },
                    datatype: 'json',
                    success: function (json) {
                        $('.client_search_input').hide();
                        $('input[name=client_id]').val(json.id);
                    }
                });
            }
        }
    });
 
    $('#client_name').on('blur', function () {
        setTimeout(function () {
            $('.client_search_input').hide();
        }, 500);
    });
 
    $('#client_name').on('keyup', function () {
        var datatype = $(this).attr('data-type');
        var q = $(this).val();
 
        if (datatype != '') {
            $.ajax({
                url: BASE_URL + '/ajax/' + datatype,
                type: 'GET',
                data: { q: q },
                dataType: 'json',
                success: function (json) {
                    if ($('.client_search_input').length == 0) {
                        $('#client_name').after('<div class="form-control mr-sm-2 client_search_input"></div>');
                    }
                    var html = '';
                    for (var item in json) {
                        html += '<div class="form-control mr-sm-2 client_search_input"><a href="javascript:;" onclick="selectClient(this)" data-id="' + json[item].id + '">' + json[item].name + '</a></div>';
                    }
                    $('.client_search_input').html(html);
                    $('.client_search_input').show();
                }
            });
        }
    });
 
    $('#add_product').on('blur', function () {
        setTimeout(function () {
            $('.searchresults_prod').hide();
        }, 500);
    });
 
    $('#add_product').on('keyup', function () {
        var datatype = $(this).attr('data-type');
        var q = $(this).val();
 
        if (datatype != '') {
            $.ajax({
                url: BASE_URL + '/ajax/' + datatype,
                type: 'GET',
                data: { q: q },
                dataType: 'json',
                success: function (json) {
                    if ($('.searchresults_prod').length == 0) {
                        $('#add_product').after('<div class="form-control mr-sm-2 searchresults_prod"></div>');
                    }
                    var html = '';
                    for (var item in json) {
                        html += '<div class="form-control mr-sm-2 searchresults_prod"><a href="javascript:;" onclick="add_product(this)" data-id="' + json[item].id + '" data-price="' + json[item].price + '" data-name="' + json[item].name + '">' + json[item].name + ' - R$ ' + json[item].price + '</a></div>';
                    }
                    $('.searchresults_prod').html(html);
                    $('.searchresults_prod').show();
                }
            });
        }
    });
});
