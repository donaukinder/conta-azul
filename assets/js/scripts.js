/* MÁSCARAS PARA OS CAMPOS */
$(function () {
	$('input[name=address_zipcode]').mask('99999-999', { placeholder: "00000-000" });
	$('input[name=phone]').mask('00 00000-0000', { placeholder: "00 00000-0000" });
	$('input[name=price]').mask("000.000.000.000.000,00", { reverse: true, placeholder: "0,00" });
});

/* BUSCA - CEP | INICIO */
$('input[name=address_zipcode]').on('blur', function () {
	var code = $(this).val();

	$.ajax({
		url: 'https://api.postmon.com.br/v1/cep/' + code,
		type: 'GET',
		dataType: 'json',
		success: function (json) {
			if (typeof json.logradouro != 'undefined') {
				$('input[name=address]').val(json.logradouro);
				$('input[name=address_neighb]').val(json.bairro);
				$('input[name=address_city]').val(json.cidade);
				$('input[name=address_state]').val(json.estado);
				$('input[name=address_country]').val("Brasil");
				$('input[name=address_number]').focus();
			}
		}
	});
});
/* BUSCA - CEP | FIM */

/* PESQUISA clients model CLIENTS */
$('#search').on('blur', function () {
	setTimeout(function () {
		$('.searchresults').hide();
	}, 500);
});

$('#search').on('keyup', function () {
	var datatype = $(this).attr('data-type');
	var q = $(this).val();

	if (datatype != '') {
		$.ajax({
			url: BASE_URL + '/ajax/' + datatype,
			type: 'GET',
			data: { q: q },
			dataType: 'json',
			success: function (json) {
				if ($('.searchresults').length == 0) {
					$('#search').after('<div class="form-control mr-sm-2 searchresults"></div>');
				}
				var html = '';
				for (var item in json) {
					html += '<div class="form-control mr-sm-2 searchresults"><a href="' + json[item].link + '">' + json[item].name + '</a></div>';
				}
				$('.searchresults').html(html);
				$('.searchresults').show();
			}
		});
	}
});