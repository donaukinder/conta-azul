<?php
require 'environment.php';

global $config;
$config = array();
if(ENVIRONMENT == 'development') {
	$config['dbname'] = 'conta-azul';
	$config['host'] = '127.0.0.1';
	$config['dbuser'] = 'admin';
	$config['dbpass'] = 'admin';
} else {
	$config['dbname'] = 'contaazul';
	$config['host'] = '127.0.0.1';
	$config['dbuser'] = 'root';
	$config['dbpass'] = 'root';
}
?>