<?php
class salesController extends controller
{

	public function __construct()
	{
		parent::__construct();

		$u = new Users();
		if ($u->isLogged() == false) {
			header("Location: " . BASE_URL . "/login");
			exit;
		}
	}

	public function index()
	{
		$data = array();
		$u = new Users();
		$u->setLoggedUser();
		$company = new Companies($u->getCompany());
		$data['company_name'] = $company->getName();
		$data['user_email'] = $u->getEmail();
		$data['status_desc'] = array(
			'0' => 'Aguardando Pagamento',
			'1' => 'Pago',
			'2' => 'Cancelado'
		);

		if ($u->hasPermission('sales_view')) {
			$sales = new Sales();
			$offset = 0;

			$data['sales_list'] = $sales->getList($offset, $u->getCompany());

			$this->loadTemplate("sales", $data);
		} else {
			header("location: " . BASE_URL);
		}
	}

	public function add()
	{
		$data = array();
		$u = new Users();
		$u->setLoggedUser();
		$company = new Companies($u->getCompany());
		$data['company_name'] = $company->getName();
		$data['user_email'] = $u->getEmail();

		if ($u->hasPermission('sales_view')) {
			$sales = new Sales();

			if (isset($_POST['client_id']) && !empty($_POST['client_id'])) {
				$client_id = addslashes($_POST['client_id']);
				$status = addslashes($_POST['status']);
				$total_price = addslashes($_POST['total_price']);

				$total_price = str_replace('.', '', $total_price);
				$total_price = str_replace(',', '.', $total_price);

				$sales->addSale($u->getCompany(), $client_id, $u->getId(), $total_price, $status);
				header("location: ".BASE_URL."/sales");
			}
			
			$this->loadTemplate("sales_add", $data);
		} else {
			header("location: " . BASE_URL);
		}
	}
}












