<!DOCTYPE html>
<html lang="pt-br">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Painel - <?php echo $viewData['company_name'];?></title>

  <!-- Custom fonts for this template-->
  <link href="<?php echo BASE_URL;?>/assets/admin/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

  <!--My Css -->
  <link rel="stylesheet" type="text/css" href="<?php echo BASE_URL;?>/assets/css/style.css">
  <!-- Page level plugin CSS-->
  <link href="<?php echo BASE_URL;?>/assets/admin/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="<?php echo BASE_URL;?>/assets/admin/css/sb-admin.css" rel="stylesheet">
<!-- Scripts -->
  <script src="<?php echo BASE_URL;?>/assets/admin/vendor/jquery/jquery.min.js"></script>
</head>

<body id="page-top">

  <nav class="navbar navbar-expand navbar-dark bg-dark static-top">

    <a class="navbar-brand mr-1" href="<?php echo BASE_URL;?>">Conta Azul</a>

    <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
      <i class="fas fa-bars"></i>
    </button>

    <!-- Spacing -->
    <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
      <div class="navbar-brand mr-1"><?php echo $viewData['user_email']; ?></div>
    </form>

    <!-- Navbar -->
    <ul class="navbar-nav ml-auto ml-md-0">
      <li class="nav-item dropdown no-arrow">
        <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-user-circle fa-fw"></i>
        </a>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
            <a class="dropdown-item" href="<?php echo BASE_URL.'/login/logout';?>" data-toggle="modal" data-target="#logoutModal">Logout</a>
        </div>
      </li>
    </ul>

  </nav>

  <div id="wrapper">
    <!-- Sidebar -->
    <ul class="sidebar navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" >
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <!-- NOME DA EMPRESA -->
          <span class="navbar-brand mr-1"><?php echo $viewData['company_name'];?></span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?php echo BASE_URL;?>">
          <i class="fas fa-fw fa-home mr-1"></i>
          <span>Home</span></a>
      </li>
       <li class="nav-item">
        <a class="nav-link" href="<?php echo BASE_URL;?>/permissions">
          <i class="fas fa-fw fa-key mr-1"></i>
          <span>Permissões</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?php echo BASE_URL;?>/users">
          <i class="fas fa-fw fa-users mr-1"></i>
          <span>Usuários</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?php echo BASE_URL;?>/clients">
          <i class="fa fa-user-tie mr-2"></i>
          <span>Clientes</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?php echo BASE_URL;?>/inventory">
         <i class="fa fa-dolly mr-2"></i>
          <span>Estoque</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?php echo BASE_URL;?>/sales">
         <i class="fa fa-cash-register mr-2"></i>
          <span>Vendas</span></a>
      </li>
    </ul>

      <?php $this->loadViewInTemplate($viewName, $viewData);?>

    </div>
    <!-- /.content-wrapper -->

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Deseja realmente sair?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Selecione "Logout" abaixo se você estiver pronto para terminar sua sessão atual.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
          <a class="btn btn-primary" href="<?php echo BASE_URL.'/login/logout';?>">Logout</a>
        </div>
      </div>
    </div>
  </div>

  
  <script type="text/javascript">var BASE_URL = '<?php echo BASE_URL;?>'</script>
  <script src="<?php echo BASE_URL;?>/assets/js/jquery.mask.min.js"></script>
  <script src="<?php echo BASE_URL;?>/assets/js/scripts.js"></script>
  <script src="<?php echo BASE_URL;?>/assets/js/scripts_sales_add.js"></script>

  <!-- Bootstrap core JavaScript-->
  <script src="<?php echo BASE_URL;?>/assets/admin/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- Core plugin JavaScript-->
  <script src="<?php echo BASE_URL;?>/assets/admin/vendor/jquery-easing/jquery.easing.min.js"></script>
  <!-- Page level plugin JavaScript-->
  <script src="<?php echo BASE_URL;?>/assets/admin/vendor/chart.js/Chart.min.js"></script>
  <script src="<?php echo BASE_URL;?>/assets/admin/vendor/datatables/jquery.dataTables.js"></script>
  <script src="<?php echo BASE_URL;?>/assets/admin/vendor/datatables/dataTables.bootstrap4.js"></script>
  <!-- Custom scripts for all pages-->
  <script src="<?php echo BASE_URL;?>/assets/admin/js/sb-admin.min.js"></script> 
</body>
</html>
