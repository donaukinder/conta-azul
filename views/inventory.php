<div class="container">
  <div class="row">
    <main role="main" class="col">
      <div class="d-flex justify-content-between flex-wrap f align-items-center pt-3 pb-2 mb-3">
        <h2>Estoque</h2>
        <?php if ($add_permission) : ?>
          <a class="btn btn-sm btn-info jus" href="<?php echo BASE_URL; ?>/inventory/add">Adicionar Produto</a>
        <?php endif; ?>
        <input class="search form-control mr-sm-2" type="text" data-type="search_inventory" id="search" placeholder="Pesquisar" aria-label="Pesquisar">
      </div>
      <div class="table-responsive">
        <table class="table table-bordered table-sm" id="dataTable">
          <thead class="thead-dark">
            <tr align="center">
              <th>Nome</th>
              <th>Preço</th>
              <th>Quantidade</th>
              <th>Quant. Min.</th>
              <th>Ações</th>
            </tr>
          </thead>
          <?php foreach ($inventory_list as $product) : ?>
            <tr align="center">
              <td><?php echo $product['name']; ?></td>
              <td>R$ <?php echo number_format($product['price'], 2, ',', '.'); ?></td>
              <td width="100"><?php echo $product['quant']; ?></td>
              <td width="100"><?php echo $product['min_quant']; ?></td>
              <td width="140">
                <div><a class="btn btn-primary float-left btn-sm" href="<?php echo BASE_URL; ?>/inventory/edit/<?php echo $product['id']; ?>">Editar</a></div>
                <div><a class="btn btn-danger btn-sm" href="<?php echo BASE_URL; ?>/inventory/delete/<?php echo $product['id']; ?>" onclick="return confirm('Realmente deseja excluir?')">Excluir</a></div>
              </td>
            </tr>
          <?php endforeach; ?>
        </table>
      </div>
    </main>
  </div>
  </div>