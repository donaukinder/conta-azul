<div class="container">
  <div class="row">
    <main role="main" class="col">
      <div class="d-flex justify-content-between flex-wrap f align-items-center pt-3 pb-2 mb-3">
        <h2>Vendas</h2>
          <a class="btn btn-sm btn-info jus" href="<?php echo BASE_URL; ?>/sales/add">Adicionar Venda</a>
      </div>
      <div class="table-responsive">
        <table class="table table-bordered table-sm" id="dataTable">
          <thead class="thead-dark">
            <tr align="center">
              <th>Cliente</th>
              <th>Data</th>
              <th>Status</th>
              <th>Valor</th>
              <th>Ações</th>
            </tr>
          </thead>
            <tr align="center">
              <?php foreach($sales_list as $sale_item): ?>
              <td><?php echo $sale_item['name'];?></td>
              <td><?php echo date('d/m/Y', strtotime($sale_item['date_sale']));?></td>
              <td width="100"><?php echo $status_desc[$sale_item['status']];?></td>
              <td width="100">R$ <?php echo number_format($sale_item['total_price'], 2, ',', '.');?></td>
              <td width="140">
                <div><a class="btn btn-primary float-left btn-sm" href="<?php echo BASE_URL; ?>/sales/edit/<?php echo $product['id']; ?>">Editar</a></div>
                <div><a class="btn btn-danger btn-sm" href="<?php echo BASE_URL; ?>/sales/delete/<?php echo $product['id']; ?>" onclick="return confirm('Realmente deseja excluir?')">Excluir</a></div>
              </td>
            </tr> 
          <?php endforeach; ?>
        </table>
      </div>
    </main>
  </div>
  </div>
