<div class="container">
  <!-- GRUPO DE PERMISSÕES -->
      <div class="row">
        <main role="main" class="col">
          <div class="d-flex justify-content-between flex-wrap f align-items-center pt-3 pb-2 mb-3"> 
          <h2>Grupo de Permissões</h2>
          <a class="btn btn-sm btn-info" href="<?php echo BASE_URL;?>/permissions/add_group">Adicionar Grupo de Permissões</a>
        </div>
          <div class="table-responsive">
              <table class="table table-bordered table-sm" id="dataTable">
                <thead class="thead-dark">
                  <tr align="center">
                    <th>Nome do Grupo de Permissões</th>
                    <th>Ações</th>
          </tr>
        <?php foreach ($permissions_groups_list as $p): ?>
          <tr align="center">
          <td><?php echo $p['name']; ?></td>
          <td width="140">
            <div><a class="btn btn-primary float-left btn-sm" href="<?php echo BASE_URL;?>/permissions/edit_group/<?php echo $p['id'];?>">Editar</a></div>
            <div><a class="btn btn-danger btn-sm" href="<?php echo BASE_URL;?>/permissions/delete_group/<?php echo $p['id'];?>" onclick="return confirm('Realmente deseja excluir?')">Excluir</a></div>
          </td>
        </tr> 
        <?php endforeach; ?>
                </thead>
              </table>
            </div>
        </main>
      </div>
      <!---GERENCIAR PERMISSÕES -->
      <div class="row">
        <main role="main" class="col">
          <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
          </div>
          <div class="d-flex justify-content-between flex-wrap f align-items-center pt-3 pb-2 mb-3 ">
          <h2 align="center">Permissões</h2>
           <a class="btn btn-sm btn-info" href="<?php echo BASE_URL;?>/permissions/add">Adicionar Permissão</a>
        </div>
          <div class="table-responsive">
              <table class="table table-bordered table-sm" id="dataTable">
                <thead class="thead-dark">
                  <tr align="center">
                    <th>Nome da Permissão</th>
                    <th>Ação</th>
				  </tr>
				  <?php foreach ($permissions_list as $p): ?>
				  <tr align="center">
					<td><?php echo $p['name']; ?></td>
					<td width="100">
            <div><a class="btn btn-danger btn-sm" href="<?php echo BASE_URL;?>/permissions/delete/<?php echo $p['id'];?>" onclick="return confirm('Realmente deseja excluir?')">Excluir</a></div>
          </td>
				</tr>	
				<?php endforeach; ?>
                </thead>
              </table>
            </div>
        </main>
      </div>
    </div>