<div class="container">
		<h1 align="center">Editar Grupo</h1>

		<form method="POST">
		<div class="form-group">
    	<label for="email">Editar Grupo de Permissões:</label>
    	<input type="text" class="form-control" name="name" value="<?php echo $group_info['name']; ?>">
    	<?php foreach ($permissions_list as $p):?>
    	<input type="checkbox" name="permissions[]"	value="<?php echo $p['id']; ?>" id="p_<?php echo $p['id'];?>"<?php echo (in_array($p['id'], $group_info['params']))?'checked="checked"':''; ?> />
    	<label for="p_<?php echo $p['id'];?>"><?php echo $p['name'];?></label><br>
    	<?php endforeach; ?>
    	<input type="submit" value="Editar" class="btn btn-success mt-3">
  		</div>
		</form>
	</div>