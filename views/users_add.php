<div class="container">
		<h1>Usuários - Adicionar</h1>

		<?php if(isset($msg_error) && !empty($msg_error)): ?>
		<div class="alert alert-danger" role="alert"><?php echo $msg_error;?></div>
		<?php endif; ?>

		<form method="POST">
		<div class="form-group">
    	<label for="email">E-mail:</label>
    	<input type="email" class="form-control" name="email" required>
  		</div>
  		<div class="form-group">
    	<label for="password">Senha:</label>
    	<input type="password" class="form-control" name="password" required>
    	</div>
    	<label for="group">Grupo de Permissão:</label>
    	<select name="group" id="group" class="custom-select" required>
    		<?php foreach($group_list as $g): ?>
		  <option value="<?php echo $g['id'];?>"><?php echo $g['name'];?></option>
		<?php endforeach;?>
		</select>
    	<input type="submit" value="Adicionar Usuário" class="btn btn-success mt-3">
		</form>
	</div>
