<div class="container">
      <div class="row">
        <main role="main" class="col">
          <div class="d-flex justify-content-between flex-wrap f align-items-center pt-3 pb-2 mb-3">  
          <h2>Clientes</h2>
           <?php if($edit_permission): ?>
            <a class="btn btn-sm btn-info jus" href="<?php echo BASE_URL;?>/clients/add">Adicionar Cliente</a>
          <?php endif; ?>
           <input class="search form-control mr-sm-2" type="text" data-type="search_clients" id="search" placeholder="Pesquisar" aria-label="Pesquisar">
          </div>
          <div class="table-responsive">
              <table class="table table-bordered table-sm" id="dataTable">
                <thead class="thead-dark">
                  <tr align="center">
                    <th>Nome</th>
                    <th>Telefone</th>
                    <th>Cidade</th>
                    <th>Estrelas</th>
                    <th>Ações</th>
        			    </tr> 
                </thead>
                <?php foreach ($clients_list as $c):?>
                    <tr align="center">
                      <td><?php echo $c['name'];?></td>
                      <td><?php echo $c['phone'];?></td>
                      <td><?php echo $c['address_city'];?></td>
                      <td><?php echo $c['stars'];?></td>
                      <td width="140">
                        <?php if($edit_permission): ?>
                         <div><a class="btn btn-primary float-left btn-sm" href="<?php echo BASE_URL;?>/clients/edit/<?php echo $c['id'];?>">Editar</a></div>
                          <div><a class="btn btn-danger btn-sm" href="<?php echo BASE_URL;?>/clients/delete/<?php echo $c['id'];?>" onclick="return confirm('Realmente deseja excluir?')">Excluir</a></div>
                        <?php else: ?>
                          <div><a class="btn btn-secondary" href="<?php echo BASE_URL;?>/clients/view/<?php echo $c['id'];?>">Visualizar</a></div>
                        <?php endif; ?>
                      </td>
                    </tr>
                 <?php endforeach;?>
              </table>
              <nav aria-label="Page navigation">
              <ul class="pagination justify-content-center">     
                <li class="page-item <?php echo ($p == 1)?'disabled':'';?>"> 
                 <?php if($p > 1):?>           
                   <a class="page-link" href="<?php echo BASE_URL;?>/clients?p=<?php echo $p-1;?>" aria-label="Anterior">
                    <span aria-hidden="true">&laquo;</span>
                  </a>
                 <?php else:?>
                  <a href="#" class="page-link" aria-label="Anterior">
                    <span aria-hidden="true">&laquo;</span>
                  </a>
                 <?php endif;?>
                </li>
                <?php for($q=1;$q<=$p_count;$q++):?>
                <li class=" page-item <?php echo ($q==$p)?'active':'';?>">
                  <a class="page-link" href="<?php echo BASE_URL;?>/clients?p=<?php echo $q;?>"><?php echo $q;?></a>
                </li>
                <?php endfor;?>
                <li class="page-item<?php echo ($p == $p_count)?'disabled':'';?>">
                <?php if($p < $p_count):?>
                  <a class="page-link " href="<?php echo BASE_URL;?>/clients?p=<?php echo $p +1;?>" aria-label="Próximo">
                    <span aria-hidden="true">&raquo;</span>
                  </a>
                  <?php else:?>
                  <a href="#" class="page-link" aria-label="Próximo">
                    <span aria-hidden="true">&raquo;</span>
                  </a>
                  <?php endif;?>
                </li>
              </ul>
          </nav>
        </div>
        </main>
      </div>
</div>