<div class="container">
	<h1>Produtos - Editar</h1>

	<?php if (isset($msg_error) && !empty($msg_error)) : ?>
		<div class="alert alert-danger" role="alert"><?php echo $msg_error; ?></div>
	<?php endif; ?>

	<form method="POST">
		<div class="form-group">
			<label for="name">Nome:</label>
			<input type="text" class="form-control" name="name" value="<?php echo $inventory_info['name']; ?>">
		</div>
		<div class="form-group">
			<label for="price">Preço:</label>
			<input type="text" class="form-control" name="price" value="<?php echo $inventory_info['price']; ?>">
		</div>
		<div class="form-group">
			<label for="quant">Quantidade:</label>
			<input type="text" class="form-control" name="quant" value="<?php echo $inventory_info['quant']; ?>">
		</div>
		<div class="form-group">
			<label for="min_quant">Quantidade Mínima:</label>
			<input type="number" class="form-control" name="min_quant" value="<?php echo $inventory_info['min_quant']; ?>">
		</div>

		<input type="submit" value="Salvar" class="btn btn-success mt-3 mb-5">
	</form>
</div>