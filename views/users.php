<div class="container">
  <!-- GRUPO DE PERMISSÕES -->
      <div class="row">
        <main role="main" class="col">
          <div class="d-flex justify-content-between flex-wrap f align-items-center pt-3 pb-2 mb-3">  
          <h2>Usuários</h2>
          <a class="btn btn-sm btn-info jus" href="<?php echo BASE_URL;?>/users/add">Adicionar Usuário</a>
          </div>
          <div class="table-responsive">
              <table class="table table-bordered table-sm" id="dataTable">
                <thead class="thead-dark">
                  <tr align="center">
                    <th>E-mail</th>
                    <th width="190">Grupo de Permissões</th>
                    <th>Ações</th>
        			    </tr>
        			<?php foreach($users_list as $us):?>
        				<tr align="center">
        					<td><?php echo $us['email']; ?></td>
        					<td><?php echo $us['name']; ?></td>
        					<td width="140">
        						 <div><a class="btn btn-primary float-left btn-sm" href="<?php echo BASE_URL;?>/users/edit/<?php echo $us['id'];?>">Editar</a></div>
            					<div><a class="btn btn-danger btn-sm" href="<?php echo BASE_URL;?>/users/delete/<?php echo $us['id'];?>" onclick="return confirm('Realmente deseja excluir?')">Excluir</a></div>
        					</td>
        				</tr>
        			<?php endforeach; ?>	
              </thead>
            </table>
          </div>
        </main>
      </div>
      </div>