<div class="container">
		<h1>Clientes - Adicionar</h1>

		<?php if(isset($msg_error) && !empty($msg_error)): ?>
		<div class="alert alert-danger" role="alert"><?php echo $msg_error;?></div>
		<?php endif; ?>

		<form method="POST">
		<div class="form-group">
	    	<label for="name">Nome:</label>
	    	<input type="text" class="form-control" name="name" required>
  		</div>	
		<div class="form-group">
	    	<label for="email">E-mail:</label>
	    	<input type="email" class="form-control" name="email">
  		</div>
  		<div class="form-group">
	    	<label for="phone">Telefone:</label>
	    	<input type="phone" class="form-control" name="phone">
    	</div>
    	<div class="form-group">
	    	<label for="address_zipcode">CEP:</label>
	    	<input type="text" class="form-control" name="address_zipcode">
    	</div>
    	<div class="form-group">
	    	<label for="address">Rua:</label>
	    	<input type="text" class="form-control" name="address">
    	</div>
    	<div class="form-group">
	    	<label for="address_number">Número:</label>
	    	<input type="text" class="form-control" name="address_number">
    	</div>
    	<div class="form-group">
	    	<label for="address2">Complemento:</label>
	    	<input type="text" class="form-control" name="address2">
    	</div>
    	<div class="form-group">
	    	<label for="address_neighb">Bairro:</label>
	    	<input type="text" class="form-control" name="address_neighb">
    	</div>
    	<div class="form-group">
	    	<label for="address_city">Cidade:</label>
	    	<input type="text" class="form-control" name="address_city">
    	</div>
    	<div class="form-group">
	    	<label for="address_state">Estado:</label>
	    	<input type="text" class="form-control" name="address_state">
    	</div>
    	<div class="form-group">
	    	<label for="address_country">País:</label>
	    	<input type="text" class="form-control" name="address_country">
    	</div>
    	<div class="form-group">
	    	<label for="stars">Classificação: </label>
	    	<select name="stars" id="stars" class="custom-select" >
	    		<option value="1"> 1 Estrela</option>
	    		<option value="2"> 2 Estrelas</option>
	    		<option value="3" selected="selected"> 3 Estrelas</option>
	    		<option value="4"> 4 Estrela</option>
	    		<option value="5"> 5 Estrela</option>
	    	</select>
    	</div>
    	<div class="form-group">
    		<label for="internal_obs">Observações:</label>
    		 <textarea class="form-control" id="internal_obs" name="internal_obs" rows="3"></textarea>
    	</div>
    	<input type="submit" value="Adicionar Cliente " class="btn btn-success mt-3 mb-5">
		</form>
	</div>
