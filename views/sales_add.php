<div class="container">
		
		<div class="d-flex justify-content-between flex-wrap f align-items-center pt-3 pb-2 mb-3">
			<h1>Vendas - Adicionar</h1>
         
      </div>
		<?php if(isset($msg_error) && !empty($msg_error)): ?>
		<div class="alert alert-danger" role="alert"><?php echo $msg_error;?></div>
		<?php endif; ?>
		<form method="POST">
		<div class="form-group mt-2">
	    	<label for="client_info">Cliente:</label>
	    	<input type="hidden" name="client_id">
	    	<button class="client_add btn btn-sm btn-info float-right mb-1"><i class="fas fa-user-plus"></i></button>
	    	<input class="search form-control mr-sm-2" type="text" data-type="search_clients" id="client_name" placeholder="Pesquisar" aria-label="Pesquisar">
		  </div>
		  <div class="input-group mb-3">
			<div class="input-group-prepend">
			<label class="input-group-text" for="status">Status da Venda</label>
			</div>
			<select class="custom-select" name="status" id="status">
			    <option value="0">Aguardando Pagamento</option>
			    <option value="1">Pago</option>
			    <option value="2">Cancelado</option>
			  </select>
			</div>
			<div class="input-group mb-3">
				<div class="input-group-prepend">
					<span class="input-group-text" for="total_price">Preço da Venda</span>
				</div>
				<input type="text" name="total_price" class="form-control" disabled="disabled">
			</div>
			<hr>
				<h4>Produtos</h4>
				<fieldset class="border p-2">
					<legend class="w-auto">Adicionar Produto</legend>
					<input class="search form-control mr-sm-2" type="text" id="add_product" data-type="search_products" placeholder="Pesquisar Produto" aria-label="Pesquisar">
				</fieldset>
			<hr>	
		<div class="table-responsive">
        <table class="table table-bordered table-sm" id="products_table">
          <thead class="thead-dark">
           <tr align="center">
			<th>Nome do Produto</th>
			<th>Quantidade</th>
			<th>Preço Unit.</th>
			<th>Sub-Total</th>
			<th>Ação</th>
            </tr>
          </thead>
		</table>
		</div>
			<hr>
			<input type="submit" value="Adicionar Venda" class="btn btn-success mt-3 mb-5">
		</form>
	</div>