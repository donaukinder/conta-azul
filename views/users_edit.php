<div class="container">
		<h1>Usuários - Editar Usuário</h1>

		<?php if(isset($msg_error) && !empty($msg_error)): ?>
		<div class="alert alert-danger" role="alert"><?php echo $msg_error;?></div>
		<?php endif; ?>

		<form method="POST">
		<div class="form-group">
    	<label for="email">E-mail:</label><br>
    	<?php echo $user_info['email']; ?>
  		</div>
  		<div class="form-group">
    	<label for="password">Senha:</label>
    	<input type="password" class="form-control" name="password">
    	</div>
    	<label for="group">Grupo de Permissão:</label>
    	<select name="group" id="group" class="custom-select" required>
    		<?php foreach($group_list as $g): ?>
		  <option value="<?php echo $g['id'];?>"<?php echo ($g['id']==$user_info['id_group'])?'selected="selected"':'';?>><?php echo $g['name'];?></option>
		<?php endforeach;?>
		</select>
    	<input type="submit" value="Editar Usuário" class="btn btn-success mt-3">
		</form>
	</div>