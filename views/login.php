<!DOCTYPE html>
<html lang="pt-br">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Login</title>

  <!-- Custom fonts for this template-->
  <link href="<?php echo BASE_URL;?>/assets/admin/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

  <!-- Custom styles for this template-->
  <link href="<?php echo BASE_URL;?>/assets/admin/css/sb-admin.css" rel="stylesheet">
  <link href="<?php echo BASE_URL;?>/assets/css/template.css" rel="stylesheet">

</head>

<body class="bg-dark">

  <div class="container">
    <div class="card card-login mx-auto mt-5">
      <div class="card-body">
        <form method="POST">
          <div class="row">
              <div class="col mb-5">
                <span class="profile-img">
                  <i class='fas fa-user-circle' style='font-size:120px'></i>
                </span>
              </div>
            </div>
          <div class="form-group">
            <div class="form-label-group">
              <input type="email" id="inputEmail" name="email" class="form-control" placeholder="Email address" required="required" autofocus="autofocus"> 
              <label for="inputEmail">Seu E-mail</label>
            </div>
          </div>
          <div class="form-group">
            <div class="form-label-group">
              <input type="password"  name="password" id="inputPassword" class="form-control" placeholder="Password" required="required">
              <label for="inputPassword">Sua Senha</label>
            </div>
          </div>
          <input type="submit" class="btn btn-primary btn-block" value="Acessar"></input>
           <?php if (isset($error) && !empty($error)):?>
          <div class="alert alert-danger mt-2" role="alert"><?php echo $error;?></div>
          <?php endif; ?>
        </form>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="<?php echo BASE_URL;?>/assets/admin/vendor/jquery/jquery.min.js"></script>
  <script src="<?php echo BASE_URL;?>/assets/admin/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="<?php echo BASE_URL;?>/assets/admin/vendor/jquery-easing/jquery.easing.min.js"></script>

</body>

</html>
