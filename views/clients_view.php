<div class="container">
		<h1>Clientes - Visualizar</h1>

		<?php if(isset($msg_error) && !empty($msg_error)): ?>
		<div class="alert alert-danger" role="alert"><?php echo $msg_error;?></div>
		<?php endif; ?>

		<form method="POST">
		<div class="form-group">
	    	<label for="name">Nome:</label>
	    	<input type="text" class="form-control" name="name" value="<?php echo $info['name'];?>" disabled>
  		</div>	
		<div class="form-group">
	    	<label for="email">E-mail:</label>
	    	<input type="email" class="form-control" name="email" value="<?php echo $info['email'];?>" disabled>
  		</div>
  		<div class="form-group">
	    	<label for="phone">Telefone:</label>
	    	<input type="phone" class="form-control" name="phone" value="<?php echo $info['phone'];?>" disabled>
    	</div>
    	<div class="form-group">
	    	<label for="address_zipcode">CEP:</label>
	    	<input type="text" class="form-control" name="address_zipcode" value="<?php echo $info['address_zipcode'];?>" disabled>
    	</div>
    	<div class="form-group">
	    	<label for="address">Rua:</label>
	    	<input type="text" class="form-control" name="address" value="<?php echo utf8_decode($info['address']);?>" disabled>
    	</div>
    	<div class="form-group">
	    	<label for="address_number">Número:</label>
	    	<input type="text" class="form-control" name="address_number" value="<?php echo $info['address_number'];?>" disabled>
    	</div>
    	<div class="form-group">
	    	<label for="address2">Complemento:</label>
	    	<input type="text" class="form-control" name="address2" value="<?php echo $info['address2'];?>" disabled>
    	</div>
    	<div class="form-group">
	    	<label for="address_neighb">Bairro:</label>
	    	<input type="text" class="form-control" name="address_neighb" value="<?php echo utf8_decode($info['address_neighb']);?>" disabled>
    	</div>
    	<div class="form-group">
	    	<label for="address_city">Cidade:</label>
	    	<input type="text" class="form-control" name="address_city" value="<?php echo $info['address_city'];?>" disabled>
    	</div>
    	<div class="form-group">
	    	<label for="address_state">Estado:</label>
	    	<input type="text" class="form-control" name="address_state" value="<?php echo $info['address_state'];?>" disabled>
    	</div>
    	<div class="form-group">
	    	<label for="address_country">País:</label>
	    	<input type="text" class="form-control" name="address_country" value="<?php echo $info['address_country'];?>" disabled>
    	</div>
    	<div class="form-group">
	    	<label for="stars">Classificação: </label>
	    	<select name="stars" id="stars" class="custom-select">
	    		<option value="1"<?php echo ($info['stars']==='1')?'selected="selected"':'';?>> 1 Estrela</option>
	    		<option value="2"<?php echo ($info['stars']==='2')?'selected="selected"':'';?>> 2 Estrelas</option>
	    		<option value="3"<?php echo ($info['stars']==='3')?'selected="selected"':'';?>> 3 Estrelas</option>
	    		<option value="4"<?php echo ($info['stars']==='4')?'selected="selected"':'';?>> 4 Estrelas</option>
	    		<option value="5"<?php echo ($info['stars']==='5')?'selected="selected"':'';?>> 5 Estrelas</option>
	    	</select>
    	</div>
    	<div class="form-group">
    		<label for="internal_obs">Observações:</label>
    		 <textarea class="form-control" id="internal_obs" name="internal_obs" rows="3" disabled><?php echo $client_info['internal_obs'];?></textarea>
    	</div>
    	<a class="btn btn-success mt-3 mb-5" href="<?php echo BASE_URL;?>/clients">Voltar</a>
		</form>
	</div>
<script src="<?php echo BASE_URL;?>/assets/js/jquery.mask.min.js"></script>
<script src="<?php echo BASE_URL;?>/assets/js/scripts.js"></script>