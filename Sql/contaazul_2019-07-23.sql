# ************************************************************
# Sequel Pro SQL dump
# Versão 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.25)
# Base de Dados: contaazul
# Tempo de Geração: 2019-07-23 15:48:28 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump da tabela clients
# ------------------------------------------------------------

DROP TABLE IF EXISTS `clients`;

CREATE TABLE `clients` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_company` int(11) NOT NULL,
  `name` varchar(100) NOT NULL DEFAULT '',
  `email` varchar(100) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `address2` varchar(100) DEFAULT NULL,
  `address_number` varchar(50) DEFAULT NULL,
  `address_neighb` varchar(100) DEFAULT NULL,
  `address_city` varchar(50) DEFAULT NULL,
  `address_state` varchar(50) DEFAULT NULL,
  `address_country` varchar(50) DEFAULT NULL,
  `address_zipcode` varchar(50) DEFAULT NULL,
  `stars` int(11) NOT NULL DEFAULT '3',
  `internal_obs` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `clients` WRITE;
/*!40000 ALTER TABLE `clients` DISABLE KEYS */;

INSERT INTO `clients` (`id`, `id_company`, `name`, `email`, `phone`, `address`, `address2`, `address_number`, `address_neighb`, `address_city`, `address_state`, `address_country`, `address_zipcode`, `stars`, `internal_obs`)
VALUES
	(12,1,'user','hcnlinux@gmail.com','92 99999-9999','Rua Canuarama (Res V Melhor','qd 35','401','Lago Azul','Manaus','AM','Brasil','69018-628',1,'testando'),
	(13,1,'Higor Carvalho','hcnlinux@gmail.com','92 99999-9999','Rua Canuarama (Res V Melhor','qd 35','401','Lago Azul','Manaus','AM','Brasil','69018-628',1,'testando'),
	(14,1,'Higor Carvalho','hcnlinux@gmail.com','92 99999-9999','Rua Canuarama (Res V Melhor','qd 35','401','Lago Azul','Manaus','AM','Brasil','69018-628',1,'testando'),
	(15,1,'Higor Carvalho','hcnlinux@gmail.com','92 99999-9999','Rua Canuarama (Res V Melhor','qd 35','401','Lago Azul','Manaus','AM','Brasil','69018-628',1,'testando'),
	(16,1,'Higor Carvalho','hcnlinux@gmail.com','92 99999-9999','Rua Canuarama (Res V Melhor','qd 35','401','Lago Azul','Manaus','AM','Brasil','69018-628',1,'testando'),
	(17,1,'Higor Carvalho','hcnlinux@gmail.com','92 99999-9999','Rua Canuarama (Res V Melhor','qd 35','401','Lago Azul','Manaus','AM','Brasil','69018-628',1,'testando'),
	(18,1,'Higor Carvalho','hcnlinux@gmail.com','92 99999-9999','Rua Canuarama (Res V Melhor','qd 35','401','Lago Azul','Manaus','AM','Brasil','69018-628',1,'testando'),
	(19,1,'Sara','chavessara21@gmail.com','92 99489-7167','Beco Dalmir Câmara','prox. ao cp bombeiros','19','Petrópolis','Manaus','AM','Brasil','69063-360',4,'Novo usuário'),
	(20,1,'donaukinder','','','','','','','','','','',3,''),
	(21,1,'hugo','','','','','','','','','','',3,''),
	(22,1,'Hugo 123','','','','','','','','','','',3,''),
	(23,1,'usuario','','','','','','','','','','',3,'');

/*!40000 ALTER TABLE `clients` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela companies
# ------------------------------------------------------------

DROP TABLE IF EXISTS `companies`;

CREATE TABLE `companies` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `companies` WRITE;
/*!40000 ALTER TABLE `companies` DISABLE KEYS */;

INSERT INTO `companies` (`id`, `name`)
VALUES
	(1,'Empresa 123'),
	(3,'Empresa 456');

/*!40000 ALTER TABLE `companies` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela inventory
# ------------------------------------------------------------

DROP TABLE IF EXISTS `inventory`;

CREATE TABLE `inventory` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_company` int(11) NOT NULL,
  `name` varchar(100) NOT NULL DEFAULT '',
  `price` float NOT NULL,
  `quant` int(11) NOT NULL,
  `min_quant` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `inventory` WRITE;
/*!40000 ALTER TABLE `inventory` DISABLE KEYS */;

INSERT INTO `inventory` (`id`, `id_company`, `name`, `price`, `quant`, `min_quant`)
VALUES
	(1,1,'Produto',250,10,15),
	(2,1,'Produto 03',2500,50,50);

/*!40000 ALTER TABLE `inventory` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela inventory_history
# ------------------------------------------------------------

DROP TABLE IF EXISTS `inventory_history`;

CREATE TABLE `inventory_history` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_company` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `action` varchar(3) NOT NULL DEFAULT '',
  `date_action` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `inventory_history` WRITE;
/*!40000 ALTER TABLE `inventory_history` DISABLE KEYS */;

INSERT INTO `inventory_history` (`id`, `id_company`, `id_product`, `id_user`, `action`, `date_action`)
VALUES
	(2,1,1,5,'add','2019-06-25 09:37:19'),
	(3,1,2,5,'add','2019-06-25 13:17:44'),
	(4,1,1,5,'edt','2019-06-29 08:57:49'),
	(5,1,1,5,'edt','2019-06-29 08:59:57'),
	(6,1,1,5,'edt','2019-06-29 09:00:01'),
	(7,1,2,5,'edt','2019-06-29 09:00:15'),
	(8,1,2,5,'edt','2019-06-29 09:00:20'),
	(9,1,1,5,'edt','2019-06-29 09:00:29'),
	(10,1,1,5,'edt','2019-06-29 09:00:33'),
	(11,1,2,5,'edt','2019-06-29 09:01:38'),
	(12,1,2,5,'edt','2019-06-29 09:02:31'),
	(13,1,2,5,'edt','2019-06-29 09:06:21'),
	(14,1,2,5,'edt','2019-06-29 09:10:11'),
	(15,1,2,5,'edt','2019-06-29 09:10:40'),
	(16,1,2,5,'edt','2019-06-29 09:13:20'),
	(17,1,1,5,'edt','2019-06-29 09:13:32'),
	(18,1,3,5,'add','2019-06-29 09:25:06'),
	(19,1,3,5,'edt','2019-06-29 09:25:30'),
	(20,1,3,5,'del','2019-06-29 09:26:53');

/*!40000 ALTER TABLE `inventory_history` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela permission_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `permission_groups`;

CREATE TABLE `permission_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_company` int(11) NOT NULL,
  `name` varchar(50) NOT NULL DEFAULT '',
  `params` varchar(200) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `permission_groups` WRITE;
/*!40000 ALTER TABLE `permission_groups` DISABLE KEYS */;

INSERT INTO `permission_groups` (`id`, `id_company`, `name`, `params`)
VALUES
	(1,1,'Desenvolvedores','1,2,7,8,9,10,11,12,13'),
	(2,1,'testadores','1,8');

/*!40000 ALTER TABLE `permission_groups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela permission_params
# ------------------------------------------------------------

DROP TABLE IF EXISTS `permission_params`;

CREATE TABLE `permission_params` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_company` int(11) NOT NULL,
  `name` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `permission_params` WRITE;
/*!40000 ALTER TABLE `permission_params` DISABLE KEYS */;

INSERT INTO `permission_params` (`id`, `id_company`, `name`)
VALUES
	(1,1,'logout'),
	(2,1,'permissions_view'),
	(7,1,'users_view'),
	(8,1,'clients_view'),
	(9,1,'clients_edit'),
	(10,1,'inventory_view'),
	(11,1,'inventory_add'),
	(12,1,'inventory_edit'),
	(13,1,'sales_view');

/*!40000 ALTER TABLE `permission_params` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela purchases
# ------------------------------------------------------------

DROP TABLE IF EXISTS `purchases`;

CREATE TABLE `purchases` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_company` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `date_purchase` datetime NOT NULL,
  `total_price` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump da tabela purchases_products
# ------------------------------------------------------------

DROP TABLE IF EXISTS `purchases_products`;

CREATE TABLE `purchases_products` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_company` int(11) NOT NULL,
  `id_purchase` int(11) NOT NULL,
  `name` varchar(100) NOT NULL DEFAULT '',
  `quant` int(11) NOT NULL,
  `purchase_price` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump da tabela sales
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sales`;

CREATE TABLE `sales` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_company` int(11) NOT NULL,
  `id_client` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `date_sale` datetime NOT NULL,
  `total_price` float NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `sales` WRITE;
/*!40000 ALTER TABLE `sales` DISABLE KEYS */;

INSERT INTO `sales` (`id`, `id_company`, `id_client`, `id_user`, `date_sale`, `total_price`, `status`)
VALUES
	(1,1,21,5,'2019-07-22 08:31:40',150,1);

/*!40000 ALTER TABLE `sales` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela sales_products
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sales_products`;

CREATE TABLE `sales_products` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_company` int(11) NOT NULL,
  `id_sale` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `quant` int(11) NOT NULL,
  `sale_price` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump da tabela users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_company` int(11) NOT NULL,
  `email` varchar(50) NOT NULL DEFAULT '',
  `password` varchar(32) NOT NULL DEFAULT '',
  `id_group` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `id_company`, `email`, `password`, `id_group`)
VALUES
	(5,1,'admin@admin.com','21232f297a57a5a743894a0e4a801fc3',1),
	(6,1,'teste@teste.com','202cb962ac59075b964b07152d234b70',2);

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
